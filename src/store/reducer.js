import {GET_CONTACTS, HIDE_MODAL, SHOW_MODAL} from "./actionTypes";

const initialState = {
    contacts: null,
    modal: 'none',
};

const reducer = (state=initialState, action) => {
    switch (action.type) {
        case GET_CONTACTS:
            return {...state, contacts: action.value,};
        case SHOW_MODAL:
            return {...state, modal: 'block'};
        case HIDE_MODAL:
            return {...state, modal: 'none'};
        default:
            return state;
    }
};

export default reducer;