import {GET_CONTACTS, HIDE_MODAL, SHOW_MODAL} from "./actionTypes";
import axios from 'axios';

export const getContacts = value => {
    return {type: GET_CONTACTS, value};
};

export const showModal = () => {
    return {type: SHOW_MODAL};
};

export const hideModal = () => {
    return {type: HIDE_MODAL};
};

export const fetchContacts = () => {
    return async dispatch => {
        try {
            const response = await axios.get('https://contacts-84e3e.firebaseio.com/contacts.json');
            dispatch(getContacts(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

