import React from 'react';
import './App.css';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import Header from "../../components/Header/Header";
import NewContact from "../../components/NewContact/NewContact";
import Contacts from "../contacts/contacts";
import Edit from "../../components/Edit/Edit";

const App = () => {
  return (
      <div className="container">
          <BrowserRouter>
              <Route component={Header}/>
              <Switch>
                  <Route path="/" exact component={Contacts}/>
                  <Route path="/newContact" exact component={NewContact}/>
                  <Route path="/edit" exact component={Edit}/>
              </Switch>
          </BrowserRouter>
      </div>
  );
};

export default App;
