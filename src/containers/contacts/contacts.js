import React, {useEffect, useState} from 'react';
import './contacts.css';
import {useDispatch, useSelector} from "react-redux";
import {fetchContacts, hideModal, showModal} from "../../store/actions";
import axios from 'axios';

const Contacts = props => {
    const contacts = useSelector(state => state.contacts);
    const modalDisplay = useSelector(state => state.modal);
    const dispatch = useDispatch();
    const [contactId, setContactId] = useState('kdjated727');

    useEffect(() => {
        dispatch(fetchContacts());
    }, [dispatch]);

    const showInfo = event => {
        setContactId(event.target.id);
        dispatch(showModal());
    };

    const closeModal = () => {
        dispatch(hideModal());
    };

    const deleteContact = async event => {
        await axios.delete('https://contacts-84e3e.firebaseio.com/contacts/'+ event.target.id +'.json');
        dispatch(hideModal());
        dispatch(fetchContacts());
    };

    const editContact = event => {
        const params = new URLSearchParams({key: event.target.value});
        props.history.push({
            pathname: '/edit',
            search: '?' + params.toString(),
        });
    };

    const modal = () => {
        if (contacts[contactId] !== undefined) {
            return(
                <div className="modal" style={{display: modalDisplay}}>
                    <button className="closeModal" onClick={closeModal}>X</button>
                    <div className="content">
                        <img className="infoImg" src={contacts[contactId].photo} alt="contact avatar"/>
                        <div className="infoInModal">
                            <h4 className="nameInModal">{contacts[contactId].name}</h4>
                            <p className="txtForLink">phone: </p><a href={'tel:' + contacts[contactId].phone} className="phoneInModal">{contacts[contactId].phone}</a>
                            <p className="txtForLink">email: </p><a href={'mailto:' + contacts[contactId].email}>{contacts[contactId].email}</a>
                        </div>
                    </div>
                    <div className="btns">
                        <button type="button" onClick={editContact} value={contactId} className="editBtn">Edit</button>
                        <button type="button" onClick={deleteContact} id={contactId} className="delBtn">Delete</button>
                    </div>
                </div>
            )
        }
    }

    if (contacts !== null) {
            return (
                <>
                    {modal()}
                    <div className="contacts">
                        {
                            Object.keys(contacts).map(key => {
                                return(
                                    <div className="contact" key={key}>
                                        <img src={contacts[key].photo} className="avatar" alt="contact avatar"/>
                                        <p className="contactName">{contacts[key].name}</p>
                                        <button onClick={showInfo} type="button" id={key} className="showInfo">Show info</button>
                                    </div>
                                )
                            })
                        }
                    </div>
                </>
            )
    } else {
        return null
    }
};

export default Contacts;