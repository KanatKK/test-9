import React, {useState} from 'react';
import './NewContact.css';
import axios from 'axios';

const NewContact = props => {
    const [sendContacts, setSendContact] = useState({
        name: '',
        phone: '',
        email: '',
        photo: '',
    })

    const contactDataChanger = event => {
        const name = event.target.name;
        const value = event.target.value;

        setSendContact(prevState => ({
            ...prevState,
            [name]: value,
        }));
    };

    const backToContacts = () => {
        props.history.push('/');
    };

    const saveContact = async () => {
        try{
            await axios.post('https://contacts-84e3e.firebaseio.com/contacts.json', {...sendContacts});
        } finally {
            props.history.push('/');
        }
    };

    return (
        <div className="addContacts">
            <h2>Add new contact</h2>
            <label>
                <p className="txtForInput">Name:</p>
                <input value={sendContacts.name} onChange={contactDataChanger} name="name" type="text"/>
            </label>
            <label>
                <p className="txtForInput">Phone:</p>
                <input value={sendContacts.phone} onChange={contactDataChanger} name="phone" type="text"/>
            </label>
            <label>
                <p className="txtForInput">Email:</p>
                <input value={sendContacts.email} onChange={contactDataChanger} name="email" type="text"/>
            </label>
            <label>
                <p className="txtForInput">Photo:</p>
                <input value={sendContacts.photo} onChange={contactDataChanger} name="photo" type="text"/>
            </label>
            <p>Photo preview:</p>
            <img className="preview" src={sendContacts.photo} alt="no"/>
            <button type="button" onClick={saveContact} className="saveBtn">Save</button>
            <button type="button" onClick={backToContacts}>Back to contacts</button>
        </div>
    );
};

export default NewContact;