import React, {useEffect, useState} from 'react';
import './Edit.css';
import axios from 'axios';

const Edit = props => {
    const [editedContact, setEditedContact] = useState({
        name: '',
        phone: '',
        email: '',
        photo: '',
    });
    const editedContactCopy = {...editedContact};

    const getParams = () => {
        const params = new URLSearchParams(props.location.search);
        return Object.fromEntries(params);
    };

    useEffect(() => {
        const edit = async () => {
            const response = await axios.get('https://contacts-84e3e.firebaseio.com/contacts/' + getParams().key + '.json');
            editedContactCopy.name = response.data.name
            editedContactCopy.phone = response.data.phone
            editedContactCopy.email = response.data.email
            editedContactCopy.photo = response.data.photo
            await setEditedContact(editedContactCopy);
        };
        edit().catch(e => {console.log(e)});
    },[]);

    const contactDataChanger = event => {
        const name = event.target.name;
        const value = event.target.value;

        setEditedContact(prevState => ({
            ...prevState,
            [name]: value,
        }));
    };

    const editContact = async () => {
        try{
            await axios.put('https://contacts-84e3e.firebaseio.com/contacts/'+ getParams().key +'.json', {...editedContact});
        } finally {
            props.history.push('/');
        }
    };

    const backToContacts = () => {
        props.history.push('/');
    };

    if (editedContact !== undefined) {
        return (
            <div className="editContact">
                <h2>Add new contact</h2>
                <label>
                    <p className="txtForInput">Name:</p>
                    <input value={editedContact.name} onChange={contactDataChanger} name="name" type="text"/>
                </label>
                <label>
                    <p className="txtForInput">Phone:</p>
                    <input value={editedContact.phone} onChange={contactDataChanger} name="phone" type="text"/>
                </label>
                <label>
                    <p className="txtForInput">Email:</p>
                    <input value={editedContact.email} onChange={contactDataChanger} name="email" type="text"/>
                </label>
                <label>
                    <p className="txtForInput">Photo:</p>
                    <input value={editedContact.photo} onChange={contactDataChanger} name="photo" type="text"/>
                </label>
                <div className="buttons">
                    <button type="button" onClick={editContact} className="editedBtn">Edit</button>
                    <button type="button" onClick={backToContacts}>Back to contacts</button>
                </div>
            </div>
        );
    } else {
        return null
    }
};

export default Edit;