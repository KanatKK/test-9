import React from 'react';
import './Header.css';

const Header = props => {
    const addContact = () => {
        props.history.push('/newContact')
    };

    return (
        <header>
            <h4>Contacts</h4>
            <button onClick={addContact} className="addBtn" type='button'>Add new contact</button>
        </header>
    );
};

export default Header;